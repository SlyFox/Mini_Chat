#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#define _GNU_SOURCE

#define ERROR_S "SERVER ERROR: "
#define PORT 1601 // номер порта
#define BUFFER_SIZE 1024
#define SYMBOL_TO_CLOSE_CONNECTION "#"
#define SYMBOL_TO_END_PROGRAM "*"
#define FILEPATH ".pid.txt"

int check_for_symbol_to_close_connection(const char* msg); // функция, которая при приеме сообщения от клиента, пробегается по этому сообщению
							   // и проверяет, прислал ли клиент завершающий символ SYMBOL_TO_CLOSE_CONNECTION или SYMBOL_TO_END_PROGRAM

void pr_handler(int sig); // обработчики сигналов
void ch_handler(int sig);
void writefile(char* string1, int file); // запись в файл

int pidslist;

int rwfile(char a, int file, int pid); // записывает (считывает) pid в файл (из файла)

int main(int argc, char* argv[]){
	time_t ticks;
	char ftime[100];
	char fname[100];
	pidslist = open(FILEPATH, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
	close(pidslist);
	pidslist = open(FILEPATH, O_RDWR | O_APPEND);
	int socket1 = socket(AF_INET, SOCK_STREAM, 0); // domain = AF_INET, домен взаимодействия процессов удаленных систем TCP/IP - определяет коммуникационный домен
					  // type = SOCK_STREAM сокет потока - тип протокола -> протокол TCP
					  // функция возвращает socket descriptor
	if (socket1 < 0){
		puts(ERROR_S "ошибка установления сокета");
		return EXIT_FAILURE;
	}
	puts("SERVER: сокет для сервера был успешно создан");
	struct sockaddr_in address1; // запись адреса
	// параметры структуры
	address1.sin_family = AF_INET;
	address1.sin_port = htons(PORT); // установка порядка байт big-endian
	address1.sin_addr.s_addr = htonl(INADDR_ANY); // установка порядка байт для IPv4; сокет будет привязан ко всем локальным интерфейсам
	int ret = bind(socket1, (struct sockaddr*)&address1, sizeof(address1)); // sockfd - дескриптор сокета socket1
								      // addr - определяет локальный адрес, с которым необходимо связать сокет, - address1
								      // addrlen - определяет размер адреса - sizeof(address1) 
								      // функция создаёт сокет, используя параметры из структуры address1
	
	if (ret < 0){
		puts(ERROR_S "операция bind не удалась");
		return EXIT_FAILURE;
	}
	
	socklen_t size = sizeof(address1);
	puts("SERVER: прослушивается порт...");
	listen(socket1, 10); // socket1 - сокет, который будет использоваться для получения запросов
			     // 10 - максимальное число запросов на установление связи, которые могут ожидать обработки сервером
	ticks = time(NULL);
	snprintf(ftime, sizeof(ftime), "%.24s", ctime(&ticks));
	snprintf(fname, sizeof(fname), "клиент-сервер_Журнал %s.txt", ftime);
	int file = open(fname, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRGRP | S_IWGRP | S_IROTH);
	close(file);
	file = open(fname, O_RDWR | O_APPEND);
	write(file, ftime, strlen(ftime));
	write(file, "\tЗапуск сервера\n", strlen("\tЗапуск сервера\n"));
	puts("Сокет создан, сервер запущен");
	puts("Символ " SYMBOL_TO_CLOSE_CONNECTION " завершает соединение\nСимвол " SYMBOL_TO_END_PROGRAM " завершает работу сервера\n");
	
	int i = 0;
	pid_t pr_pid;
	pid_t ch_pid;
	pr_pid = getpid();
	
	sigset(SIGINT, pr_handler);
	signal(SIGCHLD, SIG_IGN);
	while(true){// ожидание входящего соединения
		int newsocket = accept(socket1, (struct sockaddr*)&address1, &size); // функция создаёт новый сокет для каждого соединения, возвращает дескриптор сокета; соединение установлено
	
		////////////////////////////////////////////////////////////////обработка////////////////////////////////////////////////////////////////
		if (newsocket < 0){
			break;
		}
		
		ch_pid = fork();
		if (ch_pid == -1){
			perror("FORK: ошибка при порождении нового процесса");
		}
		else if(ch_pid == 0){ // Порождается дочерний процесс
			sigset(SIGINT, ch_handler);
			close(socket1);
			char buffer[BUFFER_SIZE];
			char frecord[100 + BUFFER_SIZE];
			send(newsocket, buffer, BUFFER_SIZE, 0); // копируем буфер в newsocket и отправляем клиенту это
			int k = i + 1;
			snprintf(frecord, sizeof(frecord), "\tК серверу подключился клиент %d\n", k);
			writefile(frecord, file);
			printf("Подключение к клиенту %d\n", k); // выводится, когда подключился клиент
			
			while (true){
				int bytes_read = recv(newsocket, buffer, BUFFER_SIZE, 0); // ожидается сообщение от клиента, в buffer будет записано сообщение от клиента
				if (bytes_read <= 0){
					printf("Клиент %d завершает работу...\n", k);
					snprintf(buffer, sizeof(buffer), "Сервер завершает работу %s", SYMBOL_TO_END_PROGRAM);
					send(newsocket, buffer, BUFFER_SIZE, 0);
					break;
				}
				snprintf(frecord, sizeof(frecord), "\tКлиент %d: %s\n", k, buffer);
				writefile(frecord, file);
				
				printf("Клиент %d: %s\n", k, buffer);
				
				int symbol = check_for_symbol_to_close_connection(buffer);
				if (symbol == 1){
					printf("Завершение работы соединения для клиента %d\n", k);
					break;
				}
				if (symbol == 2){
					kill(pr_pid, SIGINT);
				}				
			}
			close(newsocket);
			snprintf(frecord, sizeof(frecord), "\tКлиент %d отключился от сервера\n", k);
			writefile(frecord, file);
			close(file);
			exit(0);
		}
		else{
			i++;
			rwfile('w', pidslist, ch_pid);
			close(newsocket);
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	close(socket1);
	close(pidslist);
	remove(FILEPATH);
	writefile("\tЗавершение работы сервера\n", file);
	close(file);
	puts("Сервер завершил работу.");
	return EXIT_SUCCESS;
}

int check_for_symbol_to_close_connection(const char* msg){
	for (int i = 0; i < strlen(msg); i++){
		if(msg[i] == SYMBOL_TO_CLOSE_CONNECTION[0]){
			return 1;
		}
		if(msg[i] == SYMBOL_TO_END_PROGRAM[0]){
			return 2;
		}
	}
	return 0;
}

void pr_handler(int sig){
	if (sig != SIGINT){
		return;
	}
	int ch_pid;
	while ((ch_pid = rwfile('r', pidslist, 0)) != -1)
		kill(ch_pid, SIGINT);
	return;
}

void ch_handler(int sig){
	return;
}

void writefile(char* string1, int file){
	time_t ticks;
	char ftime[100];	
	ticks = time(NULL);
	snprintf(ftime, sizeof(ftime), "%.24s", ctime(&ticks));
	char frecord[100 + BUFFER_SIZE];
	snprintf(frecord, sizeof(frecord), "%s%s", ftime, string1);
	write(file, frecord, strlen(frecord));
	return;
}

int rwfile(char a, int file, int pid){
	static int pos = 0;
	struct flock lock;
	memset(&lock, 0, sizeof(lock));
	if (a == 'r'){
		lock.l_type = F_WRLCK;
	}
	else{ // a = 'w'
		lock.l_type = F_WRLCK;
		lock.l_whence = SEEK_END;
		lock.l_start = 0;
	}
	fcntl(file, F_SETLKW, &lock);
	// операция с файлом
	int n;
	if (a == 'r'){
		int i;
		char ch[7];
		memset(&ch, ' ', sizeof(ch));
		lseek(file, pos, SEEK_SET);
		i = read(file, ch, 6);
		pos += i;
		if (i == 0){
			n = -1;
		}
		else if (i < 0){
			n = -1;
			puts("Ошибка чтения pid");
		}
		else {
			n = atoi(ch);
		}
	}
	else{ // a = 'w'
		char string1[10];
		snprintf(string1, sizeof(string1), "%d\n", pid);
		char string2[7] = {0};
		for (int i = 0; i < 6 - strlen(string1); i++)
		string2[i] = '0';
		strcat(string2, string1);
		write(file, string2, strlen(string2));
		n = 0;
	}
	//
	lock.l_type = F_UNLCK;
	fcntl(file, F_SETLKW, &lock);
	return n;
}
