#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>

#define ERROR_C "CLIENT ERROR: "
#define PORT 1601 // номер порта
#define BUFFER_SIZE 1024
#define SYMBOL_TO_CLOSE_CONNECTION "#"
#define SYMBOL_TO_END_PROGRAM "*"
#define SERVER_IP "127.0.0.1"

int check_for_symbol_to_close_connection(const char* msg); // функция, которая при приеме сообщения от клиента, пробегается по этому сообщению
							   // и проверяет, прислал ли клиент завершающий символ SYMBOL_TO_CLOSE_CONNECTION или SYMBOL_TO_END_PROGRAM

void ch_handler(int sig);// обработчик сигналов

bool flag = true;

int main(int argc, char* argv[]){
	int socket1 = socket(AF_INET, SOCK_STREAM, 0); // domain = AF_INET, домен взаимодействия процессов удаленных систем TCP/IP - определяет коммуникационный домен
					  // type = SOCK_STREAM сокет потока - тип протокола -> протокол TCP
					  // protocol = 0 - должен определять используемый протокол
					  // функция возвращает socket descriptor
	if (socket1 < 0){
		puts(ERROR_C "сокет не создан.");
		return EXIT_FAILURE;
	}
	
	struct sockaddr_in address1; // запись адреса
	// параметры структуры
	address1.sin_family = AF_INET;
	address1.sin_port = htons(PORT); // установка порядка байт big-endian
	if (inet_pton(AF_INET, SERVER_IP, &address1.sin_addr) <= 0){
		puts(ERROR_C "ошибка с inet_pton.");
		return EXIT_FAILURE;
	}
	puts("CLIENT: создан клиентский сокет");
	if (connect(socket1, (struct sockaddr*)&address1, sizeof(address1)) < 0) // sockfd - дескриптор сокета socket1
								      // addr - определяет локальный адрес, с которым необходимо связать сокет, - address1
								      // addrlen - определяет размер адреса - sizeof(address1) 
								      // функция создаёт сокет, используя параметры из структуры address1
	
	{
		puts(ERROR_C "соединение не установилось");
		return EXIT_FAILURE;
	}
	
	int n;
	char buffer[BUFFER_SIZE];
	puts("Ожидание подтверждения сервера...");
	recv(socket1, buffer, BUFFER_SIZE, 0);
	puts("Соединение установлено.");
	puts("Символ " SYMBOL_TO_CLOSE_CONNECTION " завершает соединение\nСимвол " SYMBOL_TO_END_PROGRAM " завершает работу сервера\n");
	
	pid_t pr_pid;
	pid_t ch_pid;
	ch_pid = fork();
	signal(SIGCHLD, SIG_IGN);
	if (ch_pid == -1){
		perror("FORK: ошибка при порождении нового процесса");
		return EXIT_FAILURE;
	}
	else if (ch_pid == 0){
		sigset(SIGINT, ch_handler);
		while(true){
		////////////////////////////////////////////////////////////////обработка////////////////////////////////////////////////////////////////
			if (flag){
				puts("Клиент: ");
				gets(buffer);
			}
			int bytes_sent = send(socket1, buffer, BUFFER_SIZE, 0);
			if (bytes_sent <= 0){
				break;
			}
			if (check_for_symbol_to_close_connection(buffer) == 1){
				break;
			}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
		close(socket1);
		exit(0);
	}
	else{
		while(true){
			int bytes_read = recv(socket1, buffer, BUFFER_SIZE, 0);
			if (bytes_read <= 0){
				break;
			}
			puts(buffer);
			if (check_for_symbol_to_close_connection(buffer)){
				kill(ch_pid, SIGINT);
				break;
			}
		}
	}
	close(socket1);
	printf("Клиент завершил работу.\n");
	return EXIT_SUCCESS;
}

int check_for_symbol_to_close_connection(const char* msg){
	for (int i = 0; i < strlen(msg); i++){
		if(msg[i] == SYMBOL_TO_CLOSE_CONNECTION[0]){
			return 1;
		}
		if(msg[i] == SYMBOL_TO_END_PROGRAM[0]){
			return 2;
		}
	}
	return 0;
}

void ch_handler(int sig){
	if (sig != SIGINT){
		return;
	}
	flag = false;
	return;
}

